$(document).ready(function() {
    fetchPosts();

    $('#new-post-btn').click(function() {
        $('#post-form').show();
        $('#post-form-data').trigger('reset');
        $('#form-title').text('New Post');
        $('#cancel-post').show();
        $('#posts-container').hide();
    });

    $('#cancel-post').click(function() {
        hidePostForm();
    });

    $('#post-form-data').submit(function(event) {
        event.preventDefault();
        var postId = $('#post-id').val();
        var postData = {
            title: $('#post-title').val(),
            body: $('#post-body').val(),
            userId: 1 
        };
        if (postId) {
            editPost(postId, postData);
        } else {
            createPost(postData);
        }
    });

    function fetchPosts() {
        $.get("https://jsonplaceholder.typicode.com/posts", function(posts) {
            $('#post-list').empty();
            $.each(posts, function(index, post) {
                $('#post-list').append(`<li class="list-group-item" data-id="${post.id}">
                    <strong>${post.title}</strong>
                    <div>${post.body}</div>
                    <button class="btn btn-primary btn-sm edit-post">Edit</button>
                    <button class="btn btn-danger btn-sm delete-post">Delete</button>
                </li>`);
            });
        }).fail(function() {
            showError("Failed to fetch posts. Please try again later.");
        });
    }

    function createPost(postData) {
        fetch('https://jsonplaceholder.typicode.com/posts', {
            method: 'POST',
            body: JSON.stringify(postData),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        })
        .then(response => response.json())
        .then(post => {
            fetchPosts();
            hidePostForm();
        })
        .catch(() => {
            showError("Failed to create post. Please try again later.");
        });
    }

    function editPost(postId, postData) {
        fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
            method: 'PUT',
            body: JSON.stringify(postData),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        })
        .then(response => response.json())
        .then(post => {
            fetchPosts();
            hidePostForm();
        })
        .catch(() => {
            showError("Failed to edit post. Please try again later.");
        });
    }

    $(document).on('click', '.delete-post', function() {
        var postId = $(this).closest('li').data('id');
        fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
            method: 'DELETE',
        })
        .then(() => {
            fetchPosts();
        })
        .catch(() => {
            showError("Failed to delete post. Please try again later.");
        });
    });

    function hidePostForm() {
        $('#post-form').hide();
        $('#posts-container').show();
    }

    function showError(message) {
        $('#error-container').text(message).show();
    }
});
